#include "screen.hpp"

bool Screen::init(){
    bool success = true;

    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
        success = false;
    }
    else{

        window = SDL_CreateWindow(screen_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_LENGTH, SDL_WINDOW_SHOWN);

        if (window == NULL)
        {
            std::cout << "Window could not be created SDL Error: " << SDL_GetError() << std::endl;
            success = false;
        }
        else
        {            
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if(renderer == NULL)
            {
                std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
                success = false;
            }
            else
            {
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
            }
        }
    }

    return success;
}

void Screen::close(){
    SDL_DestroyRenderer(renderer); //Destroy Renderer
    SDL_DestroyWindow(window); // Destroy Window
}

Screen::Screen(const std::string &_screen_title, const int _SCREEN_WIDTH, const int _SCREEN_LENGTH) : screen_title(_screen_title), SCREEN_WIDTH(_SCREEN_WIDTH), SCREEN_LENGTH(_SCREEN_LENGTH){}

Screen::~Screen(){
    close();
}

