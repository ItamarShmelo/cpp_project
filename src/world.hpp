#ifndef WORLD
#define WORLD

#include<SDL.h>
#include<vector>
#include<limits>
#include"screen.hpp"
#include"vector3D.hpp"
#include"camera.hpp"
#include"triangle.hpp"
#include"triangle2D.hpp"



class World{
    private:
        std::vector<Triangle3D<double>> world3D;
        std::vector<Triangle2D<int>> world2D;
        std::vector<Camera> points_of_view;
        std::vector<std::vector<double>> z_buffer;
        Camera mainCamera;
        Screen* current_screen; 

    public:
        World() : world3D(), world2D(), points_of_view(), mainCamera(), current_screen(nullptr), z_buffer(){}
        ~World() {}

        void addTriangle(Triangle3D<double> &triangle) {world3D.push_back(triangle);}
        bool renderWorld(SDL_Renderer* renderer);

        void rasterizeTriangle(SDL_Renderer*  renderer, Triangle3D<double> const &tri);
        void rasterizeTopFlatTriangle(SDL_Renderer* renderer, Triangle3D<double> const &tri, Triangle2D<int> const &tri_2D);
        void rasterizeBottomFlatTriangle(SDL_Renderer* renderer,  Triangle3D<double> const &tri, Triangle2D<int> const &tri_2D);
        void rasterizeHorizontalLine(SDL_Renderer* renderer,  Triangle3D<double> const &tri , const int  x1,  int const x2, const int y);

        double depth(Triangle3D<double> const &tri, Vector3D<double> const &line);


        Camera &getCamera() { return mainCamera; } 
        
        bool createWorld(); /* Trial Function */ 
        void makeWorld2D();
        
        void change_current_screen(Screen* scr) { current_screen = scr; }
        void initialize_z_buffer(){
            
            if(current_screen == nullptr){
                std::cout << "current screen is not initialized" << std::endl;
                exit(1);
            }
            z_buffer.resize(current_screen->SCREEN_WIDTH);

            for(std::vector<double> &vec : z_buffer){
                vec.resize(current_screen->SCREEN_LENGTH);
            }

            for(std::vector<double> &vec : z_buffer){
                for(double &d : vec){
                    d = std::numeric_limits<double>::max() / 2.0;
                }
            }
        }

        void clean_z_buffer();

        

};



#endif /* WORLD */