#ifndef CAMERA
#define CAMERA

#include"vector3D.hpp"
#include<cmath>
#include <memory>


class Camera{
    public:
        std::unique_ptr<Vector3D<double>> pos;
        std::unique_ptr<Vector3D<double>> focus;
        double const distance_from_screen = 600; // Itooshs (The units of length in the program "world");

        Camera(Vector3D<double> &_pos){
            pos = std::make_unique<Vector3D<double>>(_pos);
            focus = std::make_unique<Vector3D<double>>(0.0, 0.0, 1.0);
        }
        
        
        Camera(){
            pos = std::make_unique<Vector3D<double>>(0.0, 0.0, 0.0);
            focus = std::make_unique<Vector3D<double>>(0.0, 0.0, 1.0);
        }
        
        Camera(Vector3D<double> &_pos, Vector3D<double> &_focus){
            pos = std::make_unique<Vector3D<double>>(_pos);
            focus = std::make_unique<Vector3D<double>>(_focus);

            focus->normilize();
        }

        ~Camera(){}

        void move(Vector3D<double> const &diff){ *pos += diff; }
        
        void changeFocus(Vector3D<double> &_focus) {
            *focus = _focus;
            focus->normilize();
        }
};

#endif /* CAMERA */