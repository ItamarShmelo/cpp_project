#ifndef TRIANGLE2D
#define TRIANGLE2D

#include"point.hpp"
#include<vector>

template<class T>
class Triangle2D{
    private:
        std::vector<Point<T>> triangle;
    public:
        Triangle2D(Point<T> &a, Point<T> &b, Point<T> &c) : triangle{a, b, c} {}
        Triangle2D(Point<T> const &a, Point<T> const &b, Point<T> const &c) : triangle{a, b, c} {}

        Triangle2D() : triangle{Point<T>(0,0), Point<T>(0,0), Point<T>(0,0)} {
            
        }
        ~Triangle2D() {}

        std::vector<Point<T>> get_triangle() const { return triangle; }
        void set(Point<T> const a, Point<T> const b, Point<T> const c){
            triangle[0][0] = a[0];
            triangle[0][1] = a[1];
            
            triangle[1][0] = b[0];
            triangle[1][1] = b[1];
            
            triangle[2][0] = c[0];
            triangle[2][1] = c[1];
        }

        void sort(){
            if(triangle[0][1] < triangle[1][1]){
                set(triangle[1], triangle[0], triangle[2]);
            }
            if(triangle[1][1] < triangle[2][1]){
                set(triangle[0], triangle[2], triangle[1]);
            }
            if(triangle[0][1] < triangle[1][1]){
                set(triangle[1], triangle[0], triangle[2]);
            }

        }

        void change(const int i, const int j, const int l){
            set(triangle[i], triangle[j], triangle[l]);
        }

        std::string to_string() const {
            std::string const info =    "x = " + std::to_string(triangle[0].getPoint()[0]) + " y = " + std::to_string(triangle[0].getPoint()[1]) + " \n " +
                                        "x = " + std::to_string(triangle[1].getPoint()[0]) + " y = " + std::to_string(triangle[1].getPoint()[1]) + " \n " +
                                        "x = " + std::to_string(triangle[2].getPoint()[0]) + " y = " + std::to_string(triangle[2].getPoint()[1]) + " \n ";
            return info;
        }

        const Point<T> &operator[](const int i) const {
            return triangle[i];
        }
};


#endif /* TRIANGLE2D */