#ifndef VECTOR3D
#define VECTOR3D

#include<vector>
#include<string>
#include<cmath>
#include"point.hpp"

template<class T>
class Vector3D{
    private:
        std::vector<T> vect3d;
    public:
        Vector3D(T x, T y, T z) : vect3d{x, y, z} {}

        Vector3D() : vect3d{0.0, 0.0, 0.0} {}

        void set(T x, T y, T z){
            vect3d[0] = x;
            vect3d[1] = y;
            vect3d[2] = z;
        }

        Vector3D<T> cross_product(Vector3D<T> const &temp) const{
            return Vector3D<T>(vect3d[1]*temp[2] - vect3d[2]*temp[1], vect3d[2]*temp[0] - vect3d[0]*temp[2], vect3d[0]*temp[1] - vect3d[1]*temp[0]);
        }

        Vector3D<T> cross_product(Vector3D<T> const &temp){
            return Vector3D<T>(vect3d[1]*temp[2] - vect3d[2]*temp[1], vect3d[2]*temp[0] - vect3d[0]*temp[2], vect3d[0]*temp[1] - vect3d[1]*temp[0]);
        }
        
        void operator+=(Vector3D<T> const &temp){
            vect3d[0] += temp.vect3d[0];
            vect3d[1] += temp.vect3d[1];
            vect3d[2] += temp.vect3d[2];
        }

        void set_x(T val){
            vect3d[0] = val;
        }
        Vector3D<T> operator+(Vector3D<T> const &temp1) {
            return Vector3D<T>(vect3d[0] + temp1[0], vect3d[1] + temp1[1], vect3d[2] + temp1[2]);
        }

        Vector3D<T> operator-(Vector3D<T> const &temp1) {
            return Vector3D<T>(vect3d[0] - temp1[0], vect3d[1] - temp1[1], vect3d[2] - temp1[2]);
        }

        Vector3D<T> operator-(Vector3D<T> const &temp1) const{
            return Vector3D<T>(vect3d[0] - temp1[0], vect3d[1] - temp1[1], vect3d[2] - temp1[2]);
        }

        T dot(Vector3D<T> const &temp) {
            return vect3d[0]*temp.vect3d[0] + vect3d[1]*temp.vect3d[1] + vect3d[2]*temp.vect3d[2];
        }

        T dot(Vector3D<T> const &temp) const{
            return vect3d[0]*temp.vect3d[0] + vect3d[1]*temp.vect3d[1] + vect3d[2]*temp.vect3d[2];
        }

        void scalar_multi(T const alpha){
            vect3d[0] *= alpha;
            vect3d[1] *= alpha;
            vect3d[2] *= alpha;
        }
        
        std::string to_string() const{
            std::string const info = "x = " + std::to_string(vect3d[0]) + ", y = " + std::to_string(vect3d[1]) + ", z = "  + std::to_string(vect3d[2]);  
            return info;
        }

        ~Vector3D(){}

        const std::vector<T>& get_vect3d() const{ return vect3d; }

        void normilize(){
            this->scalar_multi(1.0 / std::sqrt(this->dot(*this)));
        }

        Point<int> returnPointOnScreen(Vector3D<double> const &pos, double const distance_from_screen, double const scale_x = 1.0, double const scale_y = 1.0, int const offset_x = 0, int const offset_y = 0) const{
            double z_bar = vect3d[2] - pos.get_vect3d()[2];

            if(z_bar * z_bar < 0.000001){
                z_bar = 1.0;
            }
            const int x = offset_x + static_cast<int>( scale_x * (distance_from_screen * (vect3d[0] - pos.get_vect3d()[0]) / z_bar) );
            const int y = offset_y - static_cast<int>( scale_y * (distance_from_screen * (vect3d[1] - pos.get_vect3d()[1]) / z_bar) );
            
            return Point<int>(x, y);
        }

        T &operator[](const int i){
            return vect3d[i];
        }

        T operator[](const int i) const{
            return vect3d[i];
        }
        
};



#endif /* VECTOR3D */