#include<iostream>

#include<SDL.h>
#include<string>

#include "screen.hpp"
#include "world.hpp"

int main(int argc, char* args[]){
    Screen main_screen("Main Window", 600, 600);
    
    World world;
    world.change_current_screen(&main_screen);
    world.initialize_z_buffer();
    
    if(world.createWorld()){
        std::cout << "created world" << std::endl;
    }

    if( !main_screen.init()){
        std::cout << "Failed to initialize main screen" << std::endl;
    }
    else{
        
        bool quit = false;

        SDL_Event event_main_loop;

        Vector3D<double> const mv_right{5.0, 0.0, 0.0}; 
        Vector3D<double> const mv_left{-5.0, 0.0, 0.0}; 
        Vector3D<double> const mv_up{0.0, 5.0, 0.0};
        Vector3D<double> const mv_down{0.0, -5.0, 0.0}; 
        Vector3D<double> const mv_forward{0.0, 0.0, 5.0}; 
        Vector3D<double> const mv_backwards{0.0, 0.0, -5.0}; 



        while( !quit ){
            
            main_screen.clear_renderer();
            world.makeWorld2D();
            world.renderWorld(main_screen.get_renderer());
            main_screen.updateScreen();
            world.clean_z_buffer();
            std::cout << "update" << std::endl;
            while(SDL_PollEvent( &event_main_loop))
            {

                switch(event_main_loop.type){
                    case SDL_EventType::SDL_QUIT:
                        quit = true;
                        break;
                    case SDL_EventType::SDL_KEYDOWN:
                        switch(event_main_loop.key.keysym.sym){
                            case SDLK_RIGHT:
                                world.getCamera().move(mv_right);
                                break;
                            case SDLK_LEFT:
                                world.getCamera().move(mv_left);
                                break;
                            case SDLK_UP:
                                world.getCamera().move(mv_up);
                                break;
                            case SDLK_DOWN:
                                world.getCamera().move(mv_down);
                                break;
                            case SDLK_w:
                                world.getCamera().move(mv_forward);
                                break;
                            case SDLK_s:
                                world.getCamera().move(mv_backwards);
                                break;

                            
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    return 0;
}