#ifndef TRIANGLE
#define TRIANGLE

#include<iostream>
#include<vector>
#include"point.hpp"
#include"triangle2D.hpp"

template<class T>
class Triangle3D{
    private:
        std::vector<Vector3D<T>> triangle;
    public:
        Vector3D<T> normal;
        Triangle2D<int> tri2D;
        Triangle3D(Vector3D<T> &point_a, Vector3D<T> &point_b, Vector3D<T> &point_c) : triangle{point_a, point_b, point_c}, tri2D(), normal(){
            calculate_normal();
            std::cout << "normal :: " << normal.to_string() << std::endl;
        }
        ~Triangle3D(){}

        void calculate_normal(){
            normal = (triangle[1] - triangle[0]).cross_product(triangle[2] - triangle[0]);
            normal.normilize();
        }
        void move(Vector3D<T> &diff){
            triangle[0] += diff;
            triangle[1] += diff;
            triangle[2] += diff;
        }

        const std::string to_string() const{
            const std::string p_a = triangle[0].to_string(), p_b = triangle[1].to_string(), p_c = triangle[2].to_string();
            const std::string info = "point_a : " + p_a + "\n" + "point_b : " + p_b + "\n" + "point_c : " + p_c; 
            
            return info;
        }

         void change(const int i, const int j, const int l){
            set_by_value(triangle[i], triangle[j], triangle[l]);
        }

        void set_by_value(Vector3D<T> const a, Vector3D<T> const b, Vector3D<T> const c){
            triangle[0][0] = a[0];
            triangle[0][1] = a[1];
            triangle[0][2] = a[2];
            
            triangle[1][0] = b[0];
            triangle[1][1] = b[1];
            triangle[1][2] = b[2];
            
            triangle[2][0] = c[0];
            triangle[2][1] = c[1];
            triangle[2][2] = c[2];
        }


        void set(Vector3D<T> const &a, Vector3D<T> const &b, Vector3D<T> const &c){
            triangle[0][0] = a[0];
            triangle[0][1] = a[1];
            triangle[0][2] = a[2];
            
            triangle[1][0] = b[0];
            triangle[1][1] = b[1];
            triangle[1][2] = b[2];
            
            triangle[2][0] = c[0];
            triangle[2][1] = c[1];
            triangle[2][2] = c[2];
        }

        const Vector3D<T> &operator[](int const i) const{
            return triangle[i];
        }

        Triangle2D<int> const get2D(Camera const &camera, Screen *scr = nullptr){
            
            if(!scr){
                tri2D.set(  triangle[0].returnPointOnScreen(*camera.pos, camera.distance_from_screen),
                            triangle[1].returnPointOnScreen(*camera.pos, camera.distance_from_screen),
                            triangle[2].returnPointOnScreen(*camera.pos, camera.distance_from_screen) );
            }
            else
            {
                tri2D.set(  triangle[0].returnPointOnScreen(*camera.pos, camera.distance_from_screen, 1.0, 1.0, scr->SCREEN_WIDTH / 2., scr->SCREEN_LENGTH / 2.),
                            triangle[1].returnPointOnScreen(*camera.pos, camera.distance_from_screen, 1.0, 1.0, scr->SCREEN_WIDTH / 2., scr->SCREEN_LENGTH / 2.),
                            triangle[2].returnPointOnScreen(*camera.pos, camera.distance_from_screen, 1.0, 1.0, scr->SCREEN_WIDTH / 2., scr->SCREEN_LENGTH / 2.) );
            }
            
            return tri2D;
        }

        void sort(){
             if(tri2D[0][1] < tri2D[1][1]){
                change(1, 0, 2);
                tri2D.change(1, 0, 2);
            }
            if(tri2D[1][1] < tri2D[2][1]){
                change(0, 2, 1);
                tri2D.change(0, 2, 1);
            }
            if(tri2D[0][1] < tri2D[1][1]){
                change(1, 0, 2);
                tri2D.change(1, 0 ,2);
            }
        }

};

#endif /* TRIANGLE */