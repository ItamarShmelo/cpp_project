#ifndef SCREEN
#define SCREEN

#include<SDL.h>
#include<iostream>
#include<string>

class Screen
{
private:
    SDL_Window* window;
    SDL_Renderer* renderer;


public:

    int const SCREEN_WIDTH, SCREEN_LENGTH;
    std::string const &screen_title;

    Screen(const std::string &_screen_title, const int _SCREEN_WIDTH, const int _SCREEN_LENGTH);
    ~Screen();

    bool init(); // Initializes the window and renderer
    void close(); // Destory everything (called by ~Screen())

    SDL_Window* get_window(){return window;}
    SDL_Renderer* get_renderer(){return renderer;}
    void updateScreen() {
        std::cout << "not update screen" << std::endl;
        SDL_RenderPresent(renderer); 
        std::cout << "update screen" << std::endl;
    }
    void clear_renderer() {
        SDL_SetRenderDrawColor( renderer, 0x00, 0x00, 0x00, 0x00);
	    SDL_RenderClear( renderer );
        std::cout << "clean renderer" << std::endl;
    }

};

#endif /* SCREEN */
