#ifndef POINT
#define POINT

#include<vector>

template<class T>
class Point{
    private:
        std::vector<T> point;
    public:
        Point(T x, T y) : point{x, y} {}
        ~Point() = default;

        const std::vector<T> &getPoint() const { return point; }
        std::vector<T> &getPoint() { return point; }
        void set_point(T x, T y){ point[0] = (T)x; point[1] = (T)y;}
        const T operator[](const int i) const{
            return point[i];
        }

        T& operator[](const int i){
            return point[i];
        }

        
};

#endif /* POINT */