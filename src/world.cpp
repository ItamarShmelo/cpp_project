#include"world.hpp"


bool World::createWorld(){

    Vector3D<double> p_1(50.0, 50.0, 1000.0);
    Vector3D<double> p_2(150.0, 50.0, 1000.0);
    Vector3D<double> p_3(50.0, 150.0, 1000.0);
    Vector3D<double> p_4(150.0, 150.0, 1000.0);
    Vector3D<double> p_5(50.0, 50.0, 1100.0);

    Triangle3D<double> t1(p_1, p_2, p_3);
    Triangle3D<double> t2(p_2, p_3, p_4);
    Triangle3D<double> t3(p_1, p_2, p_5);

    addTriangle(t1);
    addTriangle(t2);
    addTriangle(t3);
    return true;
}

void World::makeWorld2D(){
    world2D.resize(world3D.size());
    
    std::size_t const size3D = world3D.size();
    
    for(int i = 0;i < size3D; ++i){
        world2D[i] = world3D[i].get2D(mainCamera, current_screen);
        world3D[i].sort();

    }
    std::cout << "make world 2D" << std::endl;
}

bool World::renderWorld(SDL_Renderer* renderer){


    std::size_t const size3D = world3D.size();
    for(int i=0; i < size3D; i++){
        if( !((i + 1 ) % 2) ){
            SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
        }
        else if (!((i + 1 ) % 3)){
            SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0xFF);
        }
        else {
            SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
        }

            std::cout << "render world" << std::endl;

            rasterizeTriangle(renderer, world3D[i]);
            std::cout << "render world" << std::endl;

                               
    }
    return true;
}

void World::rasterizeTriangle(SDL_Renderer* renderer, Triangle3D<double> const &tri){
    if(tri.tri2D[1][1] == tri.tri2D[2][1]){
        rasterizeBottomFlatTriangle(renderer, tri, tri.tri2D);
    }
    else if(tri.tri2D[0][1] == tri.tri2D[1][1]) {
        rasterizeTopFlatTriangle(renderer, tri, tri.tri2D);
    }
    else{
        const double coeff = static_cast<double>(tri[1][1] - tri[0][1]) / static_cast<double>(tri[2][1]  - tri[0][1]);
        Point<int> ver4(static_cast<int>(tri[0][0] + coeff * (tri[2][0] - tri[0][0]) ) , tri[1][1]);
        const Triangle2D<int> flat_bottom{tri.tri2D[0], tri.tri2D[1], ver4};
        const Triangle2D<int> flat_top{tri.tri2D[1], ver4, tri.tri2D[2]};

        rasterizeBottomFlatTriangle(renderer, tri, flat_bottom); 
        rasterizeTopFlatTriangle(renderer, tri, flat_top); 
    }
}

void World::rasterizeTopFlatTriangle(SDL_Renderer* renderer, Triangle3D<double> const &tri, Triangle2D<int> const &tri2D){
    const double inv_slope1 = static_cast<double>(tri2D[2][0] - tri2D[0][0]) / static_cast<double>(tri2D[2][1] - tri2D[0][1]);
    const double inv_slope2 = static_cast<double>(tri2D[2][0] - tri2D[1][0]) / static_cast<double>(tri2D[2][1] - tri2D[1][1]);

    double curx1 = tri2D[2][0], curx2 = tri2D[2][0];

    for(int scanline_y = tri2D[2][1]; scanline_y <= tri2D[1][1]; ++scanline_y)
    {
        if(curx1 < curx2){
            rasterizeHorizontalLine(renderer, tri, static_cast<int>(curx1), static_cast<int>(curx2), scanline_y);
        }
        else{
            rasterizeHorizontalLine(renderer, tri, static_cast<int>(curx2), static_cast<int>(curx1), scanline_y);
        }

        curx1 += inv_slope2;
        curx2 += inv_slope1;
    }
}

void World::rasterizeBottomFlatTriangle(SDL_Renderer* renderer, const Triangle3D<double> &tri, const Triangle2D<int> &tri2D){
    
    const double inv_slope1 = static_cast<double>(tri2D[1][0] - tri2D[0][0]) / static_cast<double>(tri2D[1][1] - tri2D[0][1]);
    const double inv_slope2 = static_cast<double>(tri2D[2][0] - tri2D[0][0]) / static_cast<double>(tri2D[2][1] - tri2D[0][1]);

    double curx1 = tri2D[0][0], curx2 = tri2D[0][0];
    

    for(int scanline_y = tri2D[0][1]; scanline_y >= tri2D[1][1]; --scanline_y)
    {   
        if(curx1 < curx2){
            rasterizeHorizontalLine(renderer, tri, static_cast<int>(curx1), static_cast<int>(curx2), scanline_y);
        }
        else{
            rasterizeHorizontalLine(renderer, tri, static_cast<int>(curx2), static_cast<int>(curx1), scanline_y);
        }
        curx1 -= inv_slope1;
        curx2 -= inv_slope2;
    }

}

void World::rasterizeHorizontalLine(SDL_Renderer* renderer, const Triangle3D<double> &tri, const int x1, const int x2, const int y){
    
    Vector3D<double> line{static_cast<double>(x1 - current_screen->SCREEN_WIDTH / 2.), static_cast<double>(current_screen->SCREEN_LENGTH / 2. - y), mainCamera.distance_from_screen};
    double z_depth = 0;
    for(int i = x1; i <= x2; ++i){
        if(i >= current_screen->SCREEN_WIDTH || i < 0){
            continue;
        }
        if(y >= current_screen->SCREEN_LENGTH || y < 0){
            continue;
        }
        line.set_x(static_cast<double>(i - current_screen->SCREEN_WIDTH / 2. + (*mainCamera.pos)[0]));
        z_depth = depth(tri, line);
        if(z_depth <= mainCamera.distance_from_screen){
            continue;
        }
        if(z_depth < z_buffer[i][y]){
            z_buffer[i][y] = z_depth;
            if(SDL_RenderDrawPoint(renderer, i, y) == -1){
                std::cout << "failed to render " << i << "  " << y << std::endl;
            }

            
        }
    }
}

double World::depth(Triangle3D<double> const &tri, Vector3D<double> const &line){
    const double d =  tri.normal.dot(tri[0] - *mainCamera.pos) / tri.normal.dot(line);
    return d * line[2];
}

void World::clean_z_buffer(){
    for(std::vector<double> &vec : z_buffer){
        for(double &d : vec){
            d = std::numeric_limits<double>::max() / 2.0;
        }
    }
}